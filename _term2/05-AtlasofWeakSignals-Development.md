---
title: Atlas of Weak Signals - Development
layout: "page"
order: 5
---


![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/84f62707bc01-64957813a9cc88ac92fde49e07c49b59.png)

### Faculty
Ramón Sangüesa      
Lucas Lorenzo Pena  

### Syllabus and Learning Objectives
* A knowledge based on harvested trends, and data.  
* Collectively built ontology and concept map.  
* Evolving knowledge base.  
* Navigable repository.    
* Possible understanding of data visualization.  

### Total Duration
Classes: 3 hours a week for 7 weeks

### Structure and Phases
* Axes of Exploration  
* Methods  
* Understanding  
* Data Tooling  
* Ontology Mapping  
* Communication  

### Output
Documented Dataset being the Atlas 

### Grading Method
Participation and data mining assignments 

### Requirements for the Students
Have a basic understanding of how social media websites and static websites work. Make an Algorithmia account.

### RAMÓN SANGÜESA
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/05425adc4b62-CV_FOTO_RamonSanguesa_S.jpg)  

### Email Address
<rsanguesa@elisava.net>

### Personal Website
[equipocafeína](http://equipocafeina.net)

### Twitter Account
@ramonsang 

### Professional BIO
Ramon Sangüesa has been doing research in Artificial Intelligence for more than 20 years specializing in Machine Learning, Uncertain Reasoning and Multiagent Systems. He is a professor at the Technical University of Catalonia (on academic leave) and now works as the Head of Technology Research at the Elisava Design and Engineering School. He has been visiting professor and researcher at Columbia University in NYC and Toronto University. He has been involved in Digital Social Innovation since long, being one of the creators of Citilab (Citizen's lab) in Barcelona and his first director of innovation. 

### LUCAS LORENZO PENA
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/1cc619b9ae25-IMG_5148.jpg)

### Email Address
<lucaslpena@gmail.com>

### Personal Website
[Lucas Lorenzo Pena](http://lucaslorenzopena.com )

### Twitter Account
@lucaslorenzop  

### Professional BIO
Lucas Pena helps innovative teams create, design, and deploy technology that impacts social experiences at the intersection of trust, governance, communities, and urban systems. 
He is a Full-Stack Software Engineer, UX Designer, and Cognitive Science & HCI Researcher currently fascinated by the symbiosis between humans and artificial intelligence as a means to empower new forms of transformative decision making.